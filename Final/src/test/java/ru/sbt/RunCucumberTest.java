package ru.sbt;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by jake on 26.05.2016.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/main/java/ru/sbt/features"},
        glue = {"ru.sbt.stepDefinitions"}
)
public class RunCucumberTest {

}
