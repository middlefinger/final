package ru.sbt.pages;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.allure.Allure;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.events.AddParameterEvent;
import ru.yandex.qatools.allure.events.StepStartedEvent;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static lib.Init.getDriver;

/**
 * Created by jake on 28.05.2016.
 */
public class VoltPage extends AnyPage {

    @FindBy(id = "filterForm")
    private WebElement form;
    @FindBy(id = "vendors-control")
    private WebElement allManufacturers;
    @FindBy(xpath = "//a[contains(text(), 'Регулировка температуры')]/..")
    private WebElement temperture;
    @FindBy(xpath = "//a[contains(text(), 'Поставляется в')]")
    private WebElement packaging;
    @FindBy(xpath = "//input[contains(@id, 'price_from')]")
    private WebElement priceFrom;
    @FindBy(xpath = "//input[contains(@id, 'price_to')]")
    private WebElement priceTo;
    @FindBy(xpath = "//div/p/span/span[contains(text(), 'Мощность')]/../../../div/p/input[1]")
    private WebElement powerFrom;
    @FindBy(xpath = "//div/p/span/span[contains(text(), 'Мощность')]/../../../div/p/input[2]")
    private WebElement powerTo;
    @FindBy(xpath = "//div/p/span/span[contains(text(), 'Макс. температура')]/../../../div/p/input[1]")
    private WebElement tempFrom;
    @FindBy(xpath = "//div/p/span/span[contains(text(), 'Макс. температура')]/../../../div/p/input[2]")
    private WebElement tempTo;
    @FindBy(xpath = "//a[contains(text(), 'Регулировка температуры')]/../../../../ul/li/span")
    private List<WebElement> temperatureList;
    @FindBy(xpath = "//span[contains(text(), 'ЖК-дисплей')]")
    private WebElement lcd;
    @FindBy(xpath = "//span[contains(text(), 'Насадки в комплекте')]")
    private WebElement tips;
    @FindBy(xpath = "//a[contains(text(), 'Поставляется в')]/../../../../ul/li/span")
    private List<WebElement> packagingList;
    @FindBy(xpath = "//span[contains(text(), 'BOSCH Professional')]")
    private WebElement professional;
    @FindBy(xpath = "//a[contains(text(), 'Следующая')]")
    private WebElement next;
    @FindBy(xpath = "//a[contains(text(), 'Предыдущая')]")
    private WebElement previous;
    public Double persent = 0.0;
//    public WebElement saleUrl = null;
    public static HashMap<String, String[]> table;
    public static HashMap<String, String> sales = new HashMap<>();

    public static final Allure lifecycle = Allure.LIFECYCLE;

    public static void readTable() {
        InputStream in;
        XSSFWorkbook wb = null;
        HashMap<String, String[]> result = new HashMap<>();

        try {
            in = new FileInputStream("src/test/java/config/criteria.xlsx");
            wb = new XSSFWorkbook(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Sheet sheet = wb.getSheetAt(0);
        Iterator<Row> rows = sheet.rowIterator();

        while (rows.hasNext()) {
            XSSFRow row = (XSSFRow) rows.next();
            XSSFCell keyCell = row.getCell(0);
            XSSFCell valueCell = row.getCell(1);
            if (keyCell != null) {
                String[] valueList = valueCell.toString().split(";");
                result.put(keyCell.toString(), valueList);
            }
        }
        table = result;
    }

    private void fill(String key, WebElement from, WebElement to) {
        String[] list = table.get(key);
        if (list != null) {
            fillField(from, list[0]);
            fillField(to, list[1]);
            lifecycle.fire(new AddParameterEvent(key, list[0]+"-"+list[1]));
        }
    }

    private void clickCheckboxes(String key, String path) {
        String[] list1 = table.get(key);
        if (list1 != null) {
            WebElement object;
            for (String element : list1) {
                object = getDriver().findElement(By.xpath(path + element + "')]"));
                tryToClick(object);
                lifecycle.fire(new AddParameterEvent("Клик на ", element));
            }
        }
    }

    private void tryToClick(WebElement element) {
        try {
            lifecycle.fire(new AddParameterEvent("Пытаемся кликнуть", element.getText()));
            click(element);
        } catch (org.openqa.selenium.WebDriverException e) {
            ((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(\"0\",\""
                    + (new Random().nextInt(200) - 100) + "\");");
            tryToClick(element);
        }
    }

    private void clickCheckbox(String key, WebElement element) {
        if (table.get(key) != null) {
            if (table.get(key)[0].equals("Отмечено")) {
                try {
                    lifecycle.fire(new AddParameterEvent("Пытаемся кликнуть", element.getText()));
                    click(element);
                } catch (org.openqa.selenium.WebDriverException e) {
                    ((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(\"0\",\""
                            + (new Random().nextInt(200) - 200) + "\");");
                    clickCheckbox(key, element);
                }
            }
        }
    }

    private void clickLink(WebElement element) {
        try {
            lifecycle.fire(new AddParameterEvent("Пытаемся кликнуть", element.getText()));
            click(element);
        } catch (org.openqa.selenium.WebDriverException e) {
            ((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(\"0\",\""
                    + (new Random().nextInt(200) - 200) + "\");");
            clickLink(element);
        }
    }

    private boolean isElementPresent(By locator) {
        try {
            getDriver().findElement(locator);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public VoltPage() {
        PageFactory.initElements(getDriver(), this);
    }

    public void find() throws InterruptedException {
        lifecycle.fire(new AddParameterEvent("Начало", "поиска"));
        fill("Цена", priceFrom, priceTo);
        fill("Мощность", powerFrom, powerTo);
        fill("Макс. температура", tempFrom, tempTo);
        clickLink(allManufacturers);
        clickCheckboxes("Производитель",
                "//div[contains(@class, 'article manufacture')]/ul/li/span/label[contains(text(), '");
        clickLink(temperture);
        clickCheckboxes("Регулировка температуры",
                "//a[contains(text(), 'Регулировка температуры')]/../../../../ul/li/span/label[contains(text(), '");
        clickLink(packaging);
        clickCheckboxes("Поставляется в",
                "//a[contains(text(), 'Поставляется в')]/../../../../ul/li/span/label[contains(text(), '");
        clickCheckbox("ЖК-дисплей", lcd);
        clickCheckbox("Насадки в комплекте", tips);
        clickCheckbox("BOSCH Professional", professional);
        form.submit();
    }

  WebElement saleUrl;
    public void checkCorrect() throws IOException {
        lifecycle.fire(new AddParameterEvent("Начало", "проверки"));
        System.out.println("checkCorrect");
        waitPageToLoad();
        String currentWindow = getDriver().getWindowHandle();
        String url = getDriver().getCurrentUrl();


        do {

            List<WebElement> items = getDriver()
                    .findElements(By
                            .xpath("//div[contains(@class, 'new-items')]/ul/li[not(contains(@class," +
                                    "'splitter'))]/div/div[contains(@class, 'new-item-list-name')]/a"));
            int count = items.size();
            WebElement link ;
            for (int i = 0; i < count; i++) {
                link = items.get(i);

                try {
                    WebElement oldPrice = link
                            .findElement(By
                                    .xpath("./../../div/div[contains(@class, 'new-item-list-price-old')]/p/noindex/span"));

                    WebElement newPrice = link
                            .findElement(By
                                    .xpath("./../../div/div[contains(@class, 'new-item-list-price-im')]/ins"));
                    Double old = Double.valueOf(oldPrice.getText().replaceAll("\\D", ""));
                    Double niu = Double.valueOf(newPrice.getText().replaceAll("\\D", ""));
                    Double sale = (old-niu)/(old/100);
                    if (sale>persent) {
                        persent = sale;
                        saleUrl = link;
                    }
                    System.out.println(link.getAttribute("href"));
                    System.out.println(newPrice.getText());
                    sales.put(link.getAttribute("href").toString(), newPrice.getText());
                }
                catch (NoSuchElementException a) {
                    System.out.println("not sale");
                }
                catch (NullPointerException e) {
                    System.out.println("sale is null");
                };

                ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView();"
                        , link);
                ((JavascriptExecutor) getDriver()).executeScript("window.open(arguments[0], true, arguments[1]);",
                        link.getAttribute("href"));
                waitPageToLoad();
                catcher(currentWindow, link);
            }

            if (isElementPresent(By.xpath("//a[contains(text(), 'Следующая')]"))) {
                getDriver().get(next.getAttribute("href"));
                VoltPage nextPage = new VoltPage();
            }
        }
        while (isElementPresent(By.xpath("//a[contains(text(), 'Следующая')]")));
        getDriver().get(url);
    }

    public void sales() {
        if (sales != null) {
            Set set = sales.entrySet();
            Iterator it = set.iterator();
            while (it.hasNext()) {
                Map.Entry me = (Map.Entry)it.next();
                lifecycle.fire(new AddParameterEvent(me.getKey().toString(), me.getValue().toString()));
            }
        }
        else {
            lifecycle.fire(new AddParameterEvent("Sales", "is NULL"));
        }
    }

    private void catcher(String currentWindow, WebElement link) {
        Set windows = getDriver().getWindowHandles();
        for (Object window : windows) {
            if (!window.equals(currentWindow)) {
                getDriver().switchTo().window(window.toString());
                DetailPage detail = new DetailPage();
                try {
                    detail.checkCriteria();
                    getDriver().close();
                    System.out.println("close" + window.toString());
                } catch (NoSuchElementException e) {
                    System.out.println("catch");
                    getDriver().close();
                    System.out.println("catch: close " + window.toString());
                    getDriver().switchTo().window(currentWindow);
                    System.out.println("switch to: "+currentWindow);
                    ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView();"
                            , link);
                    ((JavascriptExecutor) getDriver()).executeScript("window.open(arguments[0], true, arguments[1]);",
                            link.getAttribute("href"));
                    System.out.println("new window");
                    catcher(currentWindow, link);
                }
            } else {
                System.out.println("current " + currentWindow);
            }
        }
        getDriver().switchTo().window(currentWindow);
    }


    public void openMaxSalePage() {

        try {
            getDriver().get(saleUrl.getAttribute("href"));
            lifecycle.fire(new AddParameterEvent("Самая большая скидка ", persent.toString()+"%"));
            lifecycle.fire(new AddParameterEvent("На товар: ", getDriver().findElement(By.id("main-title")).getText()));
        }
        catch(NoSuchElementException e) {
            lifecycle.fire(new AddParameterEvent("Товар со скидкой","не найден"));
        }



    }
}
