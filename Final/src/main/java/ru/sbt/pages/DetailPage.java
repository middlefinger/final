package ru.sbt.pages;

import junit.framework.AssertionFailedError;
import org.apache.poi.hssf.record.BookBoolRecord;
import org.apache.xalan.xsltc.util.IntegerArray;
import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.allure.events.AddParameterEvent;

import static lib.Init.getDriver;

/**
 * Created by jake on 28.05.2016.
 */
public class DetailPage extends VoltPage {

    @FindBy(id = "price_per_m")
    private WebElement price;
    @FindBy(xpath = "//li/span[contains(text(), 'Мощность')]/../span[last()]")
    private WebElement power;
    @FindBy(xpath = "//li/span[contains(text(), 'Макс. температура')]/../span[last()]")
    private WebElement maxTemp;
    @FindBy(xpath = "//li/span[contains(text(), 'Регулировка температуры')]/../span[last()]")
    private WebElement tempControl;
    @FindBy(xpath = "//li/span[contains(text(), 'Насадки в комплекте')]/../span[last()]")
    private WebElement tips;
    @FindBy(xpath = "//li/span[contains(text(), 'Поставляется в')]/../span[last()]")
    private WebElement packaging;
    @FindBy(xpath = "//li/span[contains(text(), 'ЖК-дисплей')]/../span[last()]")
    private WebElement lcd;
    @FindBy(xpath = "//div[contains(@class, 'breadcrumbs catalog')]/ul[contains(@class, 'breadcrumbsList')]/li/a")
    private WebElement producer;
    @FindBy(id = "main-title")
    private WebElement itemTitle;

    /*public void DetailPage() {
        PageFactory.initElements(getDriver(), this);
    }*/

    private void between(WebElement element, String tab) {
        if (table.get(tab) != null) {
            Integer value = Integer.valueOf(element.getText().replaceAll("\\D", ""));
            Assert.assertTrue(tab,
                    value>=Integer.valueOf(table.get(tab)[0])&&
                    value<=Integer.valueOf(table.get(tab)[1]));
            lifecycle.fire(new AddParameterEvent(tab, "соответствует"));
        }
    }

    private void checkManufacturer() {
        if (table.get("Произодитель") != null) {
            if (!table.get("Произодитель")[0].contains("Выбрать всё")) {
                Boolean flag = false;
                for (String manufacturer : table.get("Произодитель")) {
                    if (getDriver().getTitle().contains(manufacturer)) {
                        flag = true;
                    }
                }
                Assert.assertTrue("manufacturer", flag);
            }
            lifecycle.fire(new AddParameterEvent("Производитель", "соответствует"));
        }
    }

    private void multipleCheckbox(WebElement element, String[] tab) throws  AssertionError{
        if (tab != null) {
            if (!element.getText().contains(tab[0])) {
                throw new AssertionError("multiple checkbox exception");
            }
            else {
                lifecycle.fire(new AddParameterEvent(element.getText(), tab[0]));
            }
        }
    }

    private void box(WebElement element, String[] tab) {
        if (table.get(tab) != null) {
            if (table.get(tab[0])[0].equals("Отмечено")) {
                if (!element.getText().contains("+")) {
                    throw new AssertionError("checkbox exception");
                }
                else {
                    lifecycle.fire(new AddParameterEvent(element.getText(), "соответствует"));
                }
            }
        }
    }

    private void bosch() {
        if (table.get("BOSCH Professional") != null) {
            if (!itemTitle.getText().contains("BOSCH"+"Professional")) {
                throw new AssertionError("not BOSCH Professional");
            }
        }
    }


    public void checkCriteria() throws NoSuchElementException {
        waitPageToLoad();
        lifecycle.fire(new AddParameterEvent("Зашёл на страницу", getDriver().getTitle()));
        between(price, "Цена");
        between(power, "Мощность");
        between(maxTemp, "Макс. температура");
        checkManufacturer();
        multipleCheckbox(tempControl, table.get("Регулировка температуры"));
        multipleCheckbox(packaging, table.get("Поставляется в"));
        box(lcd, table.get("ЖК-дисплей"));
        box(tips, table.get("Насадки в комплекте"));
        bosch();
    }
}
