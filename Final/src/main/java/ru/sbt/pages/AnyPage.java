package ru.sbt.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import lib.Init;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by jake on 20.05.2016.
 */
public abstract class AnyPage {
    public AnyPage() {
        PageFactory.initElements(Init.getDriver(), this);

    }
    public static void waitPageToLoad() {
        // браузер вернул страницу. JS статус complete
        ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor)driver)
                        .executeScript("return document.readyState").toString().equals("complete");
            }
        };
    }
    public void click(WebElement element) throws org.openqa.selenium.WebDriverException{
        new WebDriverWait(Init.getDriver(), 30)
                .until(ExpectedConditions.elementToBeClickable(element));
        System.out.println("click to element "+element.getText());
        element.click();
    }
    public void click(By by) {
        WebElement element = Init.getDriver().findElement(by);
        click(element);
    }
    public void assertEquals(String string, String element){
        Assert.assertEquals(string, element);
        System.out.println(string+ " equals "+element);
    }
    public void fillField(WebElement element, String string){
        element.clear();
        element.sendKeys(string);
        System.out.println("\""+string+"\" send to "+element);
    }
    public void setText(){//через вебэлемент и через бай, для селекта

    }
    public void checkbox(WebElement element) {
        click(element);
        System.out.println(element.getAttribute("value"));
    }
    public void checkbox(By by) {
        WebElement element = Init.getDriver().findElement(by);
        checkbox(element);
    }
    // селект для ненастоящего селект. для настоящих. чекбоксы
    // джавадок
// гит

}