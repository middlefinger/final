package ru.sbt.stepDefinitions;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lib.Init;
import org.openqa.selenium.WebDriver;
import ru.sbt.pages.VoltPage;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.events.AddParameterEvent;
import ru.yandex.qatools.allure.events.StepStartedEvent;
import ru.yandex.qatools.allure.events.TestSuiteFinishedEvent;
import ru.yandex.qatools.allure.events.TestSuiteStartedEvent;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static lib.Init.clearStash;
import static lib.Init.getDriver;
import static lib.Init.setStash;
import static ru.sbt.pages.AnyPage.waitPageToLoad;


/**
 * Created by jake on 27.05.2016.
 */

public class CommonStepDefinitions {

    VoltPage page;

    private WebDriver driver;
    @Before
    public void before(){
        VoltPage.lifecycle.fire(new AddParameterEvent("Begin", "of test"));
        Properties property = new Properties();
        try {
            property.load(new FileInputStream("src/test/java/config/application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for(String str : property.stringPropertyNames()) {
            setStash(str, property.getProperty(str));
        }
        this.driver = Init.getDriver();
        this.page = new VoltPage();
        VoltPage.readTable();
        System.out.println("before");

    }

    @Given("^открыть страницу '(.*)'$")
    @Title("Open main page")
    public void open(String url) throws InterruptedException {
        VoltPage.lifecycle.fire(new StepStartedEvent("open"));
        driver.get(url);
        System.out.println("+");
        waitPageToLoad();
    }

    @When("^найти подходящие товары, которые соответствуют указанным критериям$")
    @Title("Search")
    public void find() throws InterruptedException {
        page.find();
    }

    @Title("Correct")
    @Then("^убедиться, что найденные объявления соответствуют критериям")
    public void checkCorrect() throws IOException {
        page.checkCorrect();
    }

    @When("^выбрать все с пометкой 'Акция'$")
    public void sales() {
        page.sales();
    }

    @Title("Open page with max sale")
    @When("^открыть страницу товара с максимальной скидкой$")
    public void openMaxSalePage() {

        page.openMaxSalePage();
        VoltPage.lifecycle.fire(new AddParameterEvent("end", "of test"));

    }


    @After
    public void exit() {
        if (null != getDriver()){
            getDriver().quit();
        }
        clearStash();
    }
}
