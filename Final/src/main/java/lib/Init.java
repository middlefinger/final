package lib;

import cucumber.api.java.en.Given;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by jake on 17.05.2016.
 */
public class Init {
    private static HashMap<Object, Object> stash = new HashMap<>();

    private static WebDriver driver;

    @Given("устанавливаем драйвер")
    public static WebDriver getDriver() {
        if (null == driver) {
            createWebDriver();
        }
        return driver;
    }

    public static void setDriver(WebDriver driver) {

        Init.driver = driver;
    }

    public static HashMap<Object, Object> getStash() {
        if (stash == null) {
            stash = new HashMap<>();
        }
        return stash;
    }

    public static void setStash(Object key, Object value){
        stash.put(key,value);
    }

    public static void clearStash() {
        stash = null;
    }


    public static void createWebDriver() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
              switch (getStash().get("browser").toString()) {
            case "Firefox":
                capabilities.setBrowserName("firefox");
                setDriver(new FirefoxDriver(capabilities));
                break;
            case "Chrome":
                capabilities.setBrowserName("chrome");
                System.setProperty("webdriver.chrome.driver", getStash().get("webdriver.chrome.driver").toString());
                setDriver(new ChromeDriver(capabilities));
                break;
            case "IE":
                capabilities.setBrowserName("internet explorer");
                System.setProperty("webdriver.ie.driver", getStash().get("webdriver.ie.driver").toString());
                setDriver(new InternetExplorerDriver(capabilities));
                break;
        }
        driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }



}
